import demjson

def parse_file(input):
    elements = []
    with open(input, "r") as ins:
        for line in ins:
            elements.append(demjson.decode(line))

    return elements
